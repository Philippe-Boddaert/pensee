const retry = document.querySelector("#retry");
const popup = document.querySelector("#popup");
const flags = document.querySelector("#liste");

function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
}

let proposition = ["0", "0", "0", "0", "0"];

function start(e){
    document.querySelectorAll(".question")[0].classList.toggle('hidden');
    document.querySelectorAll(".question")[1].classList.toggle('hidden');
}

function nextQuestion(e){
    let button = e.currentTarget;
    let num = parseInt(button.dataset["num"], 10);
    proposition[num - 1] = button.value;
    document.querySelectorAll(".question")[num].classList.toggle('hidden');
    document.querySelectorAll(".question")[num + 1].classList.toggle('hidden');

    if (num + 1 == 6){
        document.querySelectorAll(".question")[num + 1].children[0].innerHTML = "votre drapeau est : &nbsp;<div class='flag flag-" + binToDec(proposition) + "'></div>";
    }
}

popup.addEventListener('click', function(){
    this.style.display = "none";
});

for (let ligne = 0; ligne < 4; ligne++){
    let l = document.createElement('div');
    l.classList.add('flag-container');
    for (let colonne = 0; colonne < 8; colonne++){
        let c = document.createElement('div');
        let i = (colonne + (ligne * 8));
        c.title = PAYS[i];
        c.classList.add('flag');
        c.classList.add('flag-' + i);
        l.appendChild(c);
    }
    flags.appendChild(l);
}

let q = document.createElement('div');
q.classList.add('question');
q.classList.add('block');
let t = document.createElement('div');
t.style.justifyContent = 'center';
t.style.alignItems = 'center';
t.innerHTML = "Quand vous êtes prêt, cliquer sur le bouton &nbsp; <button onclick='start()' class='button'>Lancer</button>";
q.appendChild(t);
document.querySelector("#questions").appendChild(q);

for (let i = 1; i < 6; i++){
    let q = document.createElement('div');
    q.classList.add('question');
    if (i > 0)
        q.classList.add('hidden');
    let t = document.createElement('div');
    t.innerHTML = i + ". Est-ce que vous voyez votre drapeau dans la liste ci-dessous ?";
    q.appendChild(t);
    let f = document.createElement('div');
    f.classList.add('flags');
    q.appendChild(f);
    let drapeaux = INDICE.filter((v, index) => index & Math.pow(2, 5 - i));
    shuffleArray(drapeaux);
    for (let j = 0; j < 2; j++){
        let l = document.createElement('div');
        l.classList.add('flag-container');
        for (let colonne = 0; colonne < 8; colonne++){
            let c = document.createElement('div');
            c.classList.add('flag');
            c.classList.add('flag-' + drapeaux[(8 * j) + colonne]);
            l.appendChild(c);
        }
        f.appendChild(l);
    }
    let inputs = document.createElement('div');
    inputs.classList.add("inputs");
    let oui = document.createElement('button');
    oui.value = "1";
    oui.dataset["num"] = i; 
    oui.classList.add("button");
    oui.innerHTML = "Oui";
    oui.addEventListener('click', nextQuestion);
    inputs.appendChild(oui);
    let non = document.createElement('button');
    non.value="0";
    non.dataset["num"] = i; 
    non.classList.add("button");
    non.innerHTML = "Non";
    non.addEventListener('click', nextQuestion);
    inputs.appendChild(non);
    f.appendChild(inputs);

    document.querySelector("#questions").appendChild(q);
}

q = document.createElement('div');
q.classList.add('question');
q.classList.add('hidden');
t = document.createElement('div');
t.style.justifyContent = 'center';
t.style.alignItems = 'center';
q.appendChild(t);
document.querySelector("#questions").appendChild(q);