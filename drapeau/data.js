const PAYS = {
    0: "Belgique", 
    1: "Algérie", 
    2: "Canada", 
    3: "Russie", 
    4: "Finlande", 
    5: "France", 
    6: "Afrique du Sud", 
    7: "Royaume-uni", 
    8: "Chili", 
    9: "Ouganda", 
    10: "Italie", 
    11: "Australie", 
    12: "Brésil", 
    13: "États-Unis", 
    14: "Inde", 
    15: "Chine", 
    16: "Portugal", 
    17: "Grèce", 
    18: "Vietnam", 
    19: "Thaïlande", 
    20: "Allemagne", 
    21: "Corée du Sud", 
    22: "Panama", 
    23: "Liban", 
    24: "Argentine", 
    25: "Tunisie", 
    26: "Cuba", 
    27: "Espagne", 
    28: "Islande", 
    29: "Sénégal", 
    30: "Uruguay",
    31: "Japon"};

const INDICE = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];

function decToBin(dec) {
    let remainder;
    let bin = "";

    while (dec != 0) {
        remainder = dec % 2;
        dec = Math.floor(dec / 2);
        bin += remainder;
    }

    return bin.split("").reverse().join("").padStart(5, "0");
}

function binToDec(bin){
    let dec = 0;
    let n = proposition.length;
    for (let i = 0; i < n; i++){
        dec += proposition[i] * Math.pow(2, n - 1 - i);
    }
    return dec;
}

const shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }