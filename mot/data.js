const MOTS = {0: 'collège', 1: 'nuage', 2: 'bougie', 3: 'échelle', 4: 'terrasse', 5: 'piano', 6: 'bus', 7: 'magasin', 8: 'wagon', 9: 'disque', 10: 'pluie', 11: 'vacances', 12: 'marron', 13: 'vase', 14: 'horloge', 15: 'plante', 16: 'statue', 17: 'outil', 18: 'pain', 19: 'jeu', 20: 'arbre', 21: 'chimie', 22: 'lac', 23: 'dauphin', 24: 'tournevis', 25: 'ballon', 26: 'lettre', 27: 'science', 28: 'loisir', 29: 'fleuve', 30: 'jaune', 31: 'chien', 32: 'bateau', 33: 'piscine', 34: 'carte', 35: 'multiplication', 36: 'sable', 37: 'armoire', 38: 'ski', 39: 'escalier', 40: 'page', 41: 'gomme', 42: 'gris', 43: 'rivière', 44: 'yeux', 45: 'bureau', 46: 'panneau', 47: 'volant', 48: 'micro', 49: 'cahier', 50: 'neige', 51: 'tortue', 52: 'football', 53: 'cheval', 54: 'fourmi', 55: 'valise', 56: 'siècle', 57: 'marteau', 58: 'eau', 59: 'maillot', 60: 'pin', 61: 'mouton', 62: 'politique', 63: 'nombre', 64: 'chiffre', 65: 'vélo', 66: 'chaise', 67: 'voiture', 68: 'avion', 69: 'train', 70: 'ordinateur', 71: 'lycée', 72: 'fleur', 73: 'musique', 74: 'magie', 75: 'route', 76: 'soleil', 77: 'planète', 78: 'table', 79: 'montagne', 80: 'guitare', 81: 'biologie', 82: 'mer', 83: 'meuble', 84: 'océan', 85: 'maison', 86: 'restaurant', 87: 'ville', 88: 'pays', 89: 'feuille', 90: 'continent', 91: 'rocher', 92: 'photo', 93: 'lampe', 94: 'campagne', 95: 'poisson', 96: 'animal', 97: 'air', 98: 'rouge', 99: 'casque', 100: 'professeur', 101: 'violon', 102: 'sport', 103: 'voyage', 104: 'fusée', 105: 'tableau', 106: 'arc', 107: 'vert', 108: 'stylo', 109: 'chat', 110: 'informatique', 111: 'pneu', 112: 'bleu', 113: 'chambre', 114: 'sac', 115: 'cravate', 116: 'dé', 117: 'cercle', 118: 'tapis', 119: 'porte', 120: 'dictionnaire', 121: 'mathématiques', 122: 'baccalauréat', 123: 'chaussures', 124: 'calculette', 125: 'géographie', 126: 'turquoise', 127: 'chauffage'};

const INDICE = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127];

function decToBin(dec) {
    let remainder;
    let bin = "";

    while (dec != 0) {
        remainder = dec % 2;
        dec = Math.floor(dec / 2);
        bin += remainder;
    }

    return bin.split("").reverse().join("").padStart(5, "0");
}

function binToDec(bin){
    let dec = 0;
    let n = proposition.length;
    for (let i = 0; i < n; i++){
        dec += proposition[i] * Math.pow(2, n - 1 - i);
    }
    return dec;
}

const shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }