const retry = document.querySelector("#retry");
const popup = document.querySelector("#popup");
const flags = document.querySelector("#liste");

function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
}

let proposition = ["0", "0", "0", "0", "0", "0", "0"];

function start(e){
    document.querySelector("body").children[0].classList.toggle('hidden');
    document.querySelector("#liste").classList.toggle('hidden');
    document.querySelectorAll(".question")[0].classList.toggle('hidden');
    document.querySelectorAll(".question")[1].classList.toggle('hidden');
}

function nextQuestion(e){
    let button = e.currentTarget;
    let num = parseInt(button.dataset["num"], 10);
    proposition[num - 1] = button.value;
    console.log(proposition);
    document.querySelectorAll(".question")[num].classList.toggle('hidden');
    document.querySelectorAll(".question")[num + 1].classList.toggle('hidden');

    if (num + 1 == 8){
        document.querySelectorAll(".question")[num + 1].children[0].innerHTML = "votre mot est : " + MOTS[binToDec(proposition)] + "";
    }
}

popup.addEventListener('click', function(){
    this.style.display = "none";
});

for (let ligne = 0; ligne < 13; ligne++){
    let l = document.createElement('div');
    l.classList.add('mot-container');
    for (let colonne = 0; colonne < 10; colonne++){
        let c = document.createElement('div');
        let i = (colonne + (ligne * 10));
        if (i < INDICE.length){
            c.innerHTML = MOTS[i];
            c.classList.add('mot');
            l.appendChild(c);
        }

    }
    flags.appendChild(l);
}

let q = document.createElement('div');
q.classList.add('question');
let t = document.createElement('div');
t.style.justifyContent = 'center';
t.style.alignItems = 'center';
t.innerHTML = "Quand vous êtes prêt, cliquer sur le bouton &nbsp; <button onclick='start()' class='button'>Lancer</button>";
q.appendChild(t);
document.querySelector("#questions").appendChild(q);
for (let i = 1; i < 8; i++){
    let q = document.createElement('div');
    q.classList.add('question');
    if (i > 0)
        q.classList.add('hidden');
    let f = document.createElement('div');
    f.classList.add('mots');
    q.appendChild(f);
    let mots = INDICE.filter((v, index) => index & Math.pow(2, 7 - i)).map(value => MOTS[value]);
    mots.sort();
    for (let j = 0; j < 7; j++){
        let l = document.createElement('div');
        l.classList.add('mot-container');
        for (let colonne = 0; colonne < 10; colonne++){
            let c = document.createElement('div');
            c.classList.add('mot');
            let indice = (10 * j) + colonne;
            if (indice < 64){
                c.innerHTML = mots[indice];
                l.appendChild(c);
            }
        }
        f.appendChild(l);
    }
    let inputs = document.createElement('div');
    inputs.classList.add("inputs");
    let oui = document.createElement('button');
    oui.value = "1";
    oui.dataset["num"] = i; 
    oui.classList.add("button");
    oui.innerHTML = "Oui";
    oui.addEventListener('click', nextQuestion);
    inputs.appendChild(oui);
    let non = document.createElement('button');
    non.value="0";
    non.dataset["num"] = i; 
    non.classList.add("button");
    non.innerHTML = "Non";
    non.addEventListener('click', nextQuestion);
    inputs.appendChild(non);
    let t = document.createElement('div');
    t.classList.add('block');
    t.classList.add('titre');
    t.innerHTML = "Est-ce que vous voyez votre mot dans la liste ci-dessus ?";
    f.appendChild(t);
    f.appendChild(inputs);

    document.querySelector("#questions").appendChild(q);
}
q = document.createElement('div');
q.classList.add('question');
q.classList.add('hidden');
t = document.createElement('div');
t.classList.add("solution");
q.appendChild(t);
document.querySelector("#questions").appendChild(q);